drop database block;
create database block;
use block;

create table block (
    id int auto_increment primary key,
    image varchar (255),
    titre varchar (255),
    texte varchar (255)
);

insert into block (image,titre,texte)
value ( 
    "url",
    'titre du block',
    'loremlala lalala ',
);

select* from block;

drop user if exists bob@'127.0.0.1';
create user bob@'127.0.0.1' identified by 'toto';
grant all privileges on block.*
